# isoimagewriter

Tool to write a .iso file to a USB disk

https://community.kde.org/ISOImageWriter

https://download.kde.org/unstable/isoimagewriter/

https://download.kde.org/stable/isoimagewriter/

<br>

How to clone this repository:

```
git clone https://gitlab.com/azul4/content/iso-to-usb/isoimagewriter.git
```

<br>

If you are having problems installing this package due to signature verification, please run the below before running makepkg: 

```
gpg --recv-keys E0A3EB202F8E57528E13E72FD7574483BB57B18D
```

